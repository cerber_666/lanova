<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class AksiyonlarController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for aksiyonlar
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Aksiyonlar', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $aksiyonlar = Aksiyonlar::find($parameters);
        if (count($aksiyonlar) == 0) {
            $this->flash->notice("The search did not find any aksiyonlar");

            $this->dispatcher->forward([
                "controller" => "aksiyonlar",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $aksiyonlar,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a aksiyonlar
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $aksiyonlar = Aksiyonlar::findFirstByid($id);
            if (!$aksiyonlar) {
                $this->flash->error("aksiyonlar was not found");

                $this->dispatcher->forward([
                    'controller' => "aksiyonlar",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $aksiyonlar->id;

            $this->tag->setDefault("id", $aksiyonlar->id);
            $this->tag->setDefault("tarih", $aksiyonlar->tarih);
            $this->tag->setDefault("durum", $aksiyonlar->durum);
            $this->tag->setDefault("konu", $aksiyonlar->konu);
            $this->tag->setDefault("musteri", $aksiyonlar->musteri);
            $this->tag->setDefault("ulke", $aksiyonlar->ulke);
            $this->tag->setDefault("aciklama", $aksiyonlar->aciklama);
            $this->tag->setDefault("termin", $aksiyonlar->termin);
            $this->tag->setDefault("sonuc", $aksiyonlar->sonuc);
            
        }
    }

    /**
     * Creates a new aksiyonlar
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'index'
            ]);

            return;
        }

        $aksiyonlar = new Aksiyonlar();
        $aksiyonlar->tarih = $this->request->getPost("tarih");
        $aksiyonlar->durum = $this->request->getPost("durum");
        $aksiyonlar->konu = $this->request->getPost("konu");
        $aksiyonlar->musteri = $this->request->getPost("musteri");
        $aksiyonlar->ulke = $this->request->getPost("ulke");
        $aksiyonlar->aciklama = $this->request->getPost("aciklama");
        $aksiyonlar->termin = $this->request->getPost("termin");
        $aksiyonlar->sonuc = $this->request->getPost("sonuc");
        

        if (!$aksiyonlar->save()) {
            foreach ($aksiyonlar->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("aksiyonlar was created successfully");

        $this->dispatcher->forward([
            'controller' => "aksiyonlar",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a aksiyonlar edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $aksiyonlar = Aksiyonlar::findFirstByid($id);

        if (!$aksiyonlar) {
            $this->flash->error("aksiyonlar does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'index'
            ]);

            return;
        }

        $aksiyonlar->tarih = $this->request->getPost("tarih");
        $aksiyonlar->durum = $this->request->getPost("durum");
        $aksiyonlar->konu = $this->request->getPost("konu");
        $aksiyonlar->musteri = $this->request->getPost("musteri");
        $aksiyonlar->ulke = $this->request->getPost("ulke");
        $aksiyonlar->aciklama = $this->request->getPost("aciklama");
        $aksiyonlar->termin = $this->request->getPost("termin");
        $aksiyonlar->sonuc = $this->request->getPost("sonuc");
        

        if (!$aksiyonlar->save()) {

            foreach ($aksiyonlar->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'edit',
                'params' => [$aksiyonlar->id]
            ]);

            return;
        }

        $this->flash->success("aksiyonlar was updated successfully");

        $this->dispatcher->forward([
            'controller' => "aksiyonlar",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a aksiyonlar
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $aksiyonlar = Aksiyonlar::findFirstByid($id);
        if (!$aksiyonlar) {
            $this->flash->error("aksiyonlar was not found");

            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'index'
            ]);

            return;
        }

        if (!$aksiyonlar->delete()) {

            foreach ($aksiyonlar->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "aksiyonlar",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("aksiyonlar was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "aksiyonlar",
            'action' => "index"
        ]);
    }

}
