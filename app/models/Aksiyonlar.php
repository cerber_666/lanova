<?php

class Aksiyonlar extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $tarih;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $durum;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $konu;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $musteri;

    /**
     *
     * @var string
     * @Column(type="string", length=200, nullable=false)
     */
    public $ulke;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $aciklama;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $termin;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $sonuc;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("lanova");
        $this->setSource("aksiyonlar");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'aksiyonlar';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Aksiyonlar[]|Aksiyonlar|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Aksiyonlar|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
