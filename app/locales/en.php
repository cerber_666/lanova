<?php
/**
 * Created by PhpStorm.
 * User: Volkan
 * Date: 15.12.2017
 * Time: 21:12
 */

$messages = [
    'hi'      => 'Hello',
    'bye'     => 'Good Bye',
    'hi-name' => 'Hello %name%',
    'song'    => 'This song is %song%',
];