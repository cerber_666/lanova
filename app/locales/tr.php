<?php
/**
 * Created by PhpStorm.
 * User: Volkan
 * Date: 15.12.2017
 * Time: 21:12
 */
$messages = [
    'hi'      => 'Bonjour',
    'bye'     => 'Au revoir',
    'hi-name' => 'Bonjour %name%',
    'song'    => 'La chanson est %song%',
];